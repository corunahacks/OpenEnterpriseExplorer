<?php

// src/DataFixtures/AppFixtures.php
namespace App\DataFixtures;

use App\Entity\Enterprise;
use App\Entity\Sector;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public const SECTOR_REFERENCE = 'sector';

    public function load(ObjectManager $manager)
    {
        // create 5 sectors!
        $sectorsFixtures = array ('TIC', 'Comestibles', 'Deportes', 'Sanidad', 'Institucional');
        for ($i = 0; $i < 5; $i++) {
            $sector = new Sector();
            $sector->setName($sectorsFixtures[$i]);
            $manager->persist($sector);
            // other fixtures can get this object using the UserFixtures::ADMIN_USER_REFERENCE constant
            $manager->flush();
            $reference = self::SECTOR_REFERENCE .$i;
            $this->addReference($reference, $sector);
        }


        // create 20 enterprises! Bam!
        for ($i = 0; $i < 20; $i++) {
            $enterprise = new Enterprise();
            $enterprise->setName('enterprise '.$i);
            $enterprise->setAddress('enterprise_address nº '.$i);
            $enterprise->setLegalname('legal name- '.$i);
            $enterprise->setTwitter('twitter @'.$i);
            $enterprise->setUrl('url domain w'.$i);

            // this reference returns the User object created in UserFixtures
            $aleatorio = rand(0,4);
            $enterprise->addSector($this->getReference(self::SECTOR_REFERENCE.$aleatorio));

            $manager->persist($enterprise);
        }


        $manager->flush();
    }
}