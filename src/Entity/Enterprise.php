<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ApiResource(iri="http://schema.org/Organization")
 * @ORM\Entity
 * @UniqueEntity("name")
 */
class Enterprise
{
    /**
     * @var int The entity Id
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string name of enterprise
     * @ORM\Column
     * @Assert\NotBlank
     */
    public $name = '';

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    public $legalname;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    public $address;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    public $telephone;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    public $email;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    public $url;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    public $twitter;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    public $facebook;    

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    public $linkedin;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    public $youtube;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    public $description;

    /**
     * @var string
     * @ORM\Column(type="date", nullable=true)
     */
    public $fundationDate;

    /**
     * @var string
     * @ORM\Column(type="integer", nullable=true)
     */
    public $numberOfEmployees;

    /**
     * Many Entreprise have Many Techonologies
     * @ORM\ManyToMany(targetEntity="Technology", inversedBy="enterprise")
     * @ORM\JoinTable(name="enterprise_technology")
     */
    public $technologies;
    
    /**
     * Many Entreprise have Many Sectors
     * @ORM\ManyToMany(targetEntity="Sector", inversedBy="enterprise")
     * @ORM\JoinTable(name="enterprise_sector")
     */
    public $sectors;

    /**
     * Many Entreprise have Many Cnaes
     * @ORM\ManyToMany(targetEntity="Cnae", inversedBy="enterprise")
     * @ORM\JoinTable(name="enterprise_cnae")
     */
    public $cnaes;

    public function __construct() {
        $this->technologies = new ArrayCollection();
        $this->sectors = new ArrayCollection();
        $this->cnaes = new ArrayCollection();        
    }

    public function getId(): int
    {
        return $this->id;
    }
    
    public function getName(): string
    {
        return $this->name;
    }
    
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function getLegalname(): string
    {
        return $this->legalname;
    }

    public function setLegalname(string $legalname): Enterprise
    {
        $this->legalname = $legalname;
        return $this;
    }    
    
    public function getAddress()
    {
        return $this->address;
    }
    
    public function setAddress($address)
    {
        $this->address = $address;
        return $this;
    }

    public function getTelephone()
    {
        return $this->telephone;
    }
    
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;
        return $this;
    }

    public function getEmail()
    {
        return $this->email;
    }
    
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }
    
    public function getUrl(): string
    {
        return $this->url;
    }

    public function setUrl(string $url): Enterprise
    {
        $this->url = $url;
        return $this;
    }

    public function getTwitter(): string
    {
        return $this->twitter;
    }

    public function setTwitter(string $twitter): Enterprise
    {
        $this->twitter = $twitter;
        return $this;
    }

    public function getFacebook(): string
    {
        return $this->facebook;
    }

    public function setFacebook(string $facebook): Enterprise
    {
        $this->facebook = $facebook;
        return $this;
    }

    public function getLinkedin(): string
    {
        return $this->linkedin;
    }

    public function setLinkedin(string $linkedin): Enterprise
    {
        $this->linkedin = $linkedin;
        return $this;
    }

    public function getYoutube(): string
    {
        return $this->youtube;
    }

    public function setYoutube(string $youtube): Enterprise
    {
        $this->youtube = $youtube;
        return $this;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(string $description): Enterprise
    {
        $this->description = $description;
        return $this;
    }
    public function getFundationDate(): \datetime
    {
        return $this->fundationDate;
    }

    public function setFundationDate(\datetime $fundationDate): Enterprise
    {
        $this->fundationDate = $fundationDate;
        return $this;
    }

    public function getNumberOfEmployees(): string
    {
        return $this->numberOfEmployees;
    }

    public function setNumberOfEmployees(string $numberOfEmployees): Enterprise
    {
        $this->numberOfEmployees = $numberOfEmployees;
        return $this;
    }
    
    public function getTechnologies()
    {
        return $this->technologies;
    }

    public function addTechnology(Technology $technology)
    {
        if (!$this->getTechnologies()->contains($technology)) {
            $this->technologies->add($technology);
        }
        return $this;
    }
    
    public function getSectors()
    {
        return $this->sectors;
    }

    public function addSector(Sector $sector)
    {
        if (!$this->getSectors()->contains($sector)) {
            $this->getSectors()->add($sector);
        }
        return $this;
    }

    public function getCnaes()
    {
        return $this->cnaes;
    }

    public function addCnae(Sector $cnae)
    {
        if (!$this->getCnaes()->contains($cnae)) {
            $this->getCnaes()->add($cnae);
        }
        return $this;
    }    
}