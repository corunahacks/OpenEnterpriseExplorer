<?php

namespace App\DataProvider\Exception;

class ResourceNotFoundException extends DataProviderException
{
}
