Requisitos
========

Este proyecto usa symfony 4 con api-platform

Una base de datos mysql. También tenemos un container con phpmyadmin, que puedes acceder a través de http://localhost:8777

Usa phpunit para pasar los test. Puedes ver la documentación [desde aquí](test.md).

Requiere tener docker instalado.

Y podrás ver la documentación del api desde la url:
http://localhost:8080/api/docs.html

Puedes cargar unas fixtures con el siguiente comando:
>php bin/console doctrine:fixtures:load

Para obtener la ayuda de este y cualquier comando de symfony
>php bin/console doctrine:fixtures:load --help

reference: https://symfony.com/doc/master/bundles/DoctrineFixturesBundle/index.html#loading-fixtures