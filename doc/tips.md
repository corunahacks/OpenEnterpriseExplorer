Sintaxis para crear una nueva empresa asociándole una tecnología:

Al crear una nueva empresa desde el api, a la empresa le puedes asignar una nueva teconología y un sector.

Para esto debes crearlos con anterioridad, en sus respectivadas llamadas POST.
Una vez creados. Cuando lo recuperas tendrás algo como lo siguiente:

```
{
  "@context": "/api/contexts/Sector",
  "@id": "/api/sectors",
  "@type": "hydra:Collection",
  "hydra:member": [
    {
      "@id": "/api/sectors/1",
      "@type": "Sector",
      "name": "tic",
      "id": 1,
      "enterprise": []
    }
  ],
  "hydra:totalItems": 1
}
```
El valor de @id es lo que debes de meter como referencia al crear una nueva empresa.
Ejemplo:

```
{
  "name": "Altia",
  "legalname": "Altia",
  "address": "coruña",
  "email": "contacto@altia.com",
  "url": "www.altia.es",
  "twitter": "altianews",
  "description": "Somos una compañía #digital con visión global",
  "fundationDate": "2018-05-05T13:58:45.354Z",
  "numberOfEmployees": 0,
  "technologies": ["/api/technologies/1"],
  "sectors": ["/api/sectors/1"]
}
```


